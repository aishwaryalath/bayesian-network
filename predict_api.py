#import libraries
import pandas as pd
import numpy as np
import json
import re

import ast
import io
import sys
import itertools

from pomegranate import *
from babel.numbers import format_currency
from flask import Flask, request
import requests
import logging
import time

# %%

def start_timer():
    request.start_time = time.time()

def stop_timer(response):
    resp_time = time.time() - request.start_time
    sys.stderr.write("Response time: %ss\n" % resp_time)
    return response

def record_request_data(response):
    sys.stderr.write("Request path: %s Request method: %s Response status: %s\n" % (request.path, request.method, response.status_code))
    return response

def setup_metrics(app):
    app.before_request(start_timer)
    # The order here matters since we want stop_timer
    # to be executed first
    app.after_request(record_request_data)
    app.after_request(stop_timer)
    
app = Flask(__name__)
setup_metrics(app)




# %%
d = open("bayesian_network_amt.txt", "r")
model_exact = from_json(d.read())

# %%
# %%
cols_of_model=['age_range',
 'Type_of_pregnancy', "past_history_'AMENORRHOEA'","past_history_'HYPOTHYROIDISM'","past_history_'GDM'",
"past_history_'PREVIOUS LSCS'","diagnosis_'FETAL DISTRESS'","diagnosis_'NEONATAL HYPERBILIRUBINEMIA'","diagnosis_'NEONATAL JAUNDICE'","diagnosis_'NON PROGRESS OF LABOUR'","diagnosis_'PRETERM'",
   "diagnosis_'OLIGOHYDRAMNIOS'", "procedure_'ELECTIVE LSCS'",
 "procedure_'EMERGENCY LSCS'", "procedure_'FTND'","procedure_'RMLE'","procedure_'LSCS'","procedure_'NORMAL VAGINAL DELIVERY'","procedure_'VACCUM VAGINAL DELIVERY'","procedure_'TUBECTOMY'",'total_amount_network']


# %%
age_range=["(-1.001, 27.0]","(27.0, 31.0]","(31.0, 64.0]"]

# %%
def get_the_predict_variable(jsondata):
    try:
        jsondata=json.loads(json.loads(jsondata))
        
    except:
        jsondata=json.loads(jsondata)
        
    predict=jsondata['predict']
    
    predict=list(map(lambda x:x.lower(),predict))
    
    index=[]
    for p in predict:
        ind=[i for i,x in enumerate(cols_of_model) if p in x]
        index.append(ind)
    index=sum(index,[])
    return index,predict

# %%


# %%
def find_age_bucket(x,age):
    l=x.replace('(','').replace(']','').split(',')
    l=list(map(lambda x:float(x),l))
    if age>l[0] and age<=l[1]:
        return x
    else:
        return None

# %%
def prepare_data_from_input(jsondata):
    try:
        jsondata=json.loads(json.loads(jsondata))
        
    except:
        jsondata=json.loads(jsondata)
    

    input_data=dict()
    
    diagnosis=jsondata['diagnosis']
    diagnosis=list(map(lambda x:x.upper(),diagnosis))
    
    past_history=jsondata['past_history']
    past_history=list(map(lambda x:x.upper(),past_history))
    
    pregnancy=jsondata['pregnancy_type']
    pregnancy=list(map(lambda x:x.upper(),pregnancy))
    
    procedure=jsondata['procedure']
    procedure=list(map(lambda x:x.upper(),procedure))
    
    symptoms=jsondata['symptoms']
    symptoms=list(map(lambda x:x.upper(),symptoms))
    
    room_cat=jsondata['room_category']
    
    room_cat=room_cat.upper()
    room_cat="room_category_'"+str(room_cat)+"'"
    
    los=jsondata['los']
    
    age=jsondata['age']
    
    if not age=='None':
    
        bucket=list(map(lambda x:find_age_bucket(x,age),age_range))
        bucket=list(filter(None,bucket))  
        input_data['age_range']=bucket[0]
        
        
#     input_data[-1]=los

#     if room_cat in cols_of_model:
#         ind=[i for i,x in enumerate(cols_of_model) if x == room_cat]
#         input_data[ind[0]]=1
        
    for j in diagnosis:
        d="diagnosis_'"+str(j)+"'"
        if d in cols_of_model:
            ind=[i for i,x in enumerate(cols_of_model) if x == d]
            input_data[cols_of_model[ind[0]]]=1
    
#     for i in symptoms:
#         d="symptoms_'"+str(i)+"'"
#         if d in cols_of_model:
#             ind=[i for i,x in enumerate(cols_of_model) if x == d]
#             input_data[ind[0]]=1
            
    for j in past_history:
        d="past_history_'"+str(j)+"'"
        if d in cols_of_model:
            ind=[i for i,x in enumerate(cols_of_model) if x == d]
            input_data[cols_of_model[ind[0]]]=1
    
    for j in procedure:
        d="procedure_'"+str(j)+"'"
        if d in cols_of_model:
            ind=[i for i,x in enumerate(cols_of_model) if x == d]
            input_data[cols_of_model[ind[0]]]=1
            
    for i in pregnancy:
        input_data['Type_of_pregnancy']=i
            
    return input_data



# App trigger
@app.route("/all", methods=['GET', 'POST'])
def wrapper_1():
    req = request.get_data()
    data=prepare_data_from_input(req)
    
    res = model_exact.predict_proba(data)
    
    output=dict()
    for i in range(0,len(cols_of_model)):
        if cols_of_model[i] in list(data.keys()):
            output[cols_of_model[i]]=res[i]
            try:
                output[cols_of_model[i]]=round(output[cols_of_model[i]],3)
            except:
                pass
        else:
            output[cols_of_model[i]]=res[i].parameters[0]
            output[cols_of_model[i]]={key:round(output[cols_of_model[i]][key],3) for key in output[cols_of_model[i]]}
            if cols_of_model[i]=='total_amount_network' or cols_of_model[i]=='age_range':
                output[cols_of_model[i]]=sorted(output[cols_of_model[i]].items(), key = lambda kv:(kv[1], kv[0]),reverse=True)
                output[cols_of_model[i]]=dict(output[cols_of_model[i]])
                output[cols_of_model[i]]={key:str(round(output[cols_of_model[i]][key]*100,2))+' %' for key in output[cols_of_model[i]]}
                
                for k in list(output[cols_of_model[i]].keys()):
                    l=k.replace('(','').replace(']','').split(',')
                    l=list(map(lambda x:round(float(x),3),l))
                    l=str(l)
                    output[cols_of_model[i]][l]=output[cols_of_model[i]].pop(k)
                    
            if cols_of_model[i]=='Type_of_pregnancy':
                output[cols_of_model[i]]=sorted(output[cols_of_model[i]].items(), key = lambda kv:(kv[1], kv[0]),reverse=True)
                output[cols_of_model[i]]=dict(output[cols_of_model[i]])
                
            
            try:
#                 output[cols_of_model[i]]['do not occur']=output[cols_of_model[i]].pop(0)
                del output[cols_of_model[i]][0]
                output[cols_of_model[i]]=output[cols_of_model[i]].pop(1)
                output[cols_of_model[i]]=str(round(output[cols_of_model[i]]*100,2))+' %'
            except:
                pass
            
        
    
    output=json.dumps(output)
    return output

@app.route("/specific", methods=['GET', 'POST'])
def wrapper_2():
    req = request.get_data()
    data=prepare_data_from_input(req)
    
    to_predict_indexes,predictors=get_the_predict_variable(req)
    
    res = model_exact.predict_proba(data)
    
    output=dict()
    
    for i in to_predict_indexes:
        if cols_of_model[i] in list(data.keys()):
            output[cols_of_model[i]]=res[i]
            try:
                output[cols_of_model[i]]=round(output[cols_of_model[i]],3)
            except:
                pass
        else:
            output[cols_of_model[i]]=res[i].parameters[0]
            output[cols_of_model[i]]={key:round(output[cols_of_model[i]][key],3) for key in output[cols_of_model[i]]}
            if cols_of_model[i]=='total_amount_network' or cols_of_model[i]=='age_range':
                output[cols_of_model[i]]=sorted(output[cols_of_model[i]].items(), key = lambda kv:(kv[1], kv[0]),reverse=True)
                output[cols_of_model[i]]=dict(output[cols_of_model[i]])
                output[cols_of_model[i]]={key:str(round(output[cols_of_model[i]][key]*100,2))+' %' for key in output[cols_of_model[i]]}
                
                for k in list(output[cols_of_model[i]].keys()):
                    l=k.replace('(','').replace(']','').split(',')
                    l=list(map(lambda x:round(float(x),3),l))
                    l=str(l)
                    output[cols_of_model[i]][l]=output[cols_of_model[i]].pop(k)
                    
            if cols_of_model[i]=='Type_of_pregnancy':
                output[cols_of_model[i]]=sorted(output[cols_of_model[i]].items(), key = lambda kv:(kv[1], kv[0]),reverse=True)
                output[cols_of_model[i]]=dict(output[cols_of_model[i]])
            
            try:
#                 output[cols_of_model[i]]['do not occur']=output[cols_of_model[i]].pop(0)
                del output[cols_of_model[i]][0]
                output[cols_of_model[i]]=output[cols_of_model[i]].pop(1)
                output[cols_of_model[i]]=round(output[cols_of_model[i]]*100,2)
            except:
                pass
            
            
            
    features={}
    for i in predictors:
    
        features[i]={key : val for key, val in output.items() if i in key}
        
        if i=='past_history' or i=='diagnosis' or i=="procedure":
            
            features[i]=dict(sorted(features[i].items(), key = lambda kv:(kv[1], str(kv[0])+" %"),reverse=True))
            features[i]={key:str(features[i][key])+' %' for key in features[i]}
            for j in list(features[i].keys()):
                temp1=j
                temp1=temp1.split("_")
                if len(temp1)==2:
                    temp1=temp1[1]
                elif len(temp1)==3:
                    temp1=temp1[2]
                temp1=ast.literal_eval(temp1)
                features[i][temp1]=features[i].pop(j)
                
        if i=='total_amount':
            for j in list(features[i]['total_amount_network'].keys()):
                temp=j
                temp=ast.literal_eval(temp)
                temp=list(map(lambda x:round(x),temp))
                temp=list(map(lambda x:format_currency(x, 'INR', locale='en_IN'),temp))
                temp=list(map(lambda x:x.replace(".00",""),temp))
                temp=str(temp)
                temp=temp.replace("'","")
                features[i]['total_amount_network'][temp]=features[i]['total_amount_network'].pop(j)
            features[i]=features[i]['total_amount_network']
            
            
    feat=json.dumps(features)
    return feat

    

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5066, debug=False)




# %%
