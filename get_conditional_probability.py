# %%
import pandas as pd
import numpy as np


# %%
def calculate_discrete_probability(col,data):
    discrete_prob = data.groupby(col).size().div(len(data))
    return discrete_prob


# %%
def calculate_intersection(list_of_cols,data):
    intersection=data.groupby(list_of_cols).size().div(len(data))
    return intersection

# %%
def calculate_conditional_probability(joint,given):
    conditional_prob=joint.div(given, axis=0)
    return conditional_prob

# %%
def calculate_counts(list_of_cols,data,prob_data):
    g=data.groupby(list_of_cols).size().reset_index(name='counts')
    prob_data=pd.concat([prob_data,g['counts']],axis=1)
    return prob_data

# %%
def calculate_prob(data,event_happened,event_will_occur):
    if len(event_happened)==1:
        discrete_prob=calculate_discrete_probability(event_happened[0],data)
        intersection=calculate_intersection([event_happened[0],event_will_occur],data)
        conditional_prob=calculate_conditional_probability(intersection,discrete_prob)
        d=pd.DataFrame(conditional_prob).reset_index()
        d=d.rename(columns={0:'actual_probability'})
        d=calculate_counts([event_happened[0],event_will_occur],data,d)
        ind=data.columns.tolist().index(event_will_occur)
        
        data_dict={}
        event_happened.append(event_will_occur)
        data_dict[str(event_happened)]=d
        data_dict['index']=ind
        
        return data_dict
    else:
        
        intersection1=calculate_intersection(event_happened+[event_will_occur],data)
        intersection2=calculate_intersection(event_happened,data)
        conditional_prob=calculate_conditional_probability(intersection1,intersection2)
        d=pd.DataFrame(conditional_prob).reset_index()
        d=d.rename(columns={0:'actual_probability'})
        d=calculate_counts(event_happened+[event_will_occur],data,d)
        ind=data.columns.tolist().index(event_will_occur)
        
        data_dict={}
        event_happened.append(event_will_occur)
        data_dict[str(event_happened)]=d
        data_dict['index']=ind
        
        return data_dict

# %%
