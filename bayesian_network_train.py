# %%
#import libraries
import pandas as pd
import numpy as np
import json
import re
from dateutil.relativedelta import relativedelta
from datetime import date 
%matplotlib inline
import matplotlib.pyplot as plt
from matplotlib import cm

import ast
import pymysql
import matplotlib.image as mpimg

import io
import sys
import itertools
from pomegranate import *
from pomegranate.utils import plot_networkx
import networkx


# %%
"""
database connection
"""

# %%
def database(db_name):

    data_b = pymysql.connect(
                        host='production-do-user-6971464-0.a.db.ondigitalocean.com',
                        port = 25060,
                        user='sureclaim',
                        passwd='c6ohrzu9sa0j0aq9',
                        db=db_name)
    return data_b

#database encode
def encode(db):
    encoding = "latin1"
    return lambda x: str(x, encoding,errors='ignore')

#connection to sureclaim
db=database('sureclaim')
#to avoid unicode decode error
db.text_factory = encode(db)

# %%
"""
# WHOLE MATERNITY DATA
"""

# %%
motherhood_sarjapur=pd.read_csv('maternity_data - motherhood sarjapur (2).csv')

# %%
motherhood_sarjapur.shape

# %%
all_hosp=pd.read_csv('maternity_data - all hospitals with data (2).csv')

# %%
all_hosp.shape

# %%
motherhood_data=pd.concat([motherhood_sarjapur,all_hosp],axis=0)

# %%
motherhood_data.shape

# %%
motherhood_data['card_info_from_insurance_details']=motherhood_data['card_info_from_insurance_details'].replace('F140',None)

# %%
#function to get individual fields from card info
def policy_data_collection(row):
    if str(row['card_info_from_insurance_details'])!='nan' and row['card_info_from_insurance_details']!=None:

        try:
            s=ast.literal_eval(row['card_info_from_insurance_details'])
        except:
            try:
                s=json.loads(row['card_info_from_insurance_details'])
            except:
                s=row['card_info_from_insurance_details']
                
        
        s=json.loads(s[0])
        ts=s['treatmentSublimit']
        wp=s['waitingPeriod']
        phe=s['preHospitalisationExpense']
        pohe=s['postHospitalisationExpense']
        mcs=s['treatmentApplicableInfo']
        try:
            mcs=json.loads(mcs[0])
            mcs1=mcs['maternityCSectionSublimit']
        except:
            mcs1=None
        tr=s['treatmentRemarks']
        return pd.Series([ts,wp,phe,pohe,mcs1,tr],index=['Treatment_sublimit','waiting_period','pre_hosp_expense_in_days','post_hosp_expense_in_days','maternity_c_Section_sublimit','treatment_remarks'])
    else:
        return pd.Series([None,None,None,None,None,None],index=['Treatment_sublimit','waiting_period','pre_hosp_expense_in_days','post_hosp_expense_in_days','maternity_c_Section_sublimit','treatment_remarks'])

# %%
motherhood_data[['Treatment_sublimit','waiting_period','pre_hosp_expense_in_days','post_hosp_expense_in_days','maternity_c_Section_sublimit','treatment_remarks']]=motherhood_data.apply(lambda x:policy_data_collection(x),axis=1)

# %%
#reading settlement letter table
settlement_data=pd.read_sql('Select * from settlement_letter_parsed_data',con=db)
settlement_data=settlement_data[['uid_claim_id','reasons_for_deduction','total_claim_amount', 'total_deducted_amount', 'total_settled_amount',
       'predicted_data','deduction_reasons']]
settlement_data=settlement_data.add_suffix('_from_settlement_letter_table')
settlement_data=settlement_data.rename(columns={'uid_claim_id_from_settlement_letter_table':'uid_claim_id'})
#merging with motherhood data
motherhood_data=motherhood_data.merge(settlement_data,on='uid_claim_id',how='left')

# %%
motherhood_data.shape

# %%
#getting nan columns
nan_cols = [i for i in motherhood_data.columns if motherhood_data[i].isnull().all()==True or (motherhood_data[i]==0).all()==True or motherhood_data[i].isin([np.nan,0]).all()==True]
nan_cols

# %%
#function to check whether admission and discharge date are within policy period
def flag_for_treatment_in_policy_duration(row):
    try:
        dd=pd.to_datetime(row['discharge_date_from_claim_details'],errors='coerce')
        ad=pd.to_datetime(row['admission_date_from_claim_details'],errors='coerce')
        psd=pd.to_datetime(row['policyStartDate_from_insurance_details'],errors='coerce')
        ped=pd.to_datetime(row['policyEndDate_from_insurance_details'],errors='coerce')
        if (dd>=psd and dd<=ped) and (ad>=psd and ad<=ped):
            return 1
        else:
            return 0
    except:
        return None
motherhood_data['flag_for_admission_period_within_policy_period']=motherhood_data.apply(lambda x:flag_for_treatment_in_policy_duration(x),axis=1)
motherhood_data['flag_for_admission_period_within_policy_period'].value_counts()

# %%
#checking if columns are in appropriate data type
motherhood_data.columns.to_series().groupby(motherhood_data.dtypes).groups

# %%
#function to process each row having list of complications
def reform_list(x):
    if str(x)!='nan' and not x==None:
        x=x.replace('[','')
        x=x.replace(']','')
        x=x.replace("'",'')
        x=x.replace('.',',')
        x=x.replace('FIBROID UTERUS','FIBROID UTERUS SURGERY')
        x=x.replace('LATENT PHASE OF LABOUR','LATENT STAGE OF LABOUR')
        x=x.replace('FIBROID UTERUS SURGERY SURGERY','FIBROID UTERUS SURGERY')
        x=x.replace('AMENORHOEA','AMENORRHOEA')
        x=x.replace('HYPOGLYCEMIA','ASYMPTOMATIC HYPOGLYCEMIA')
        x=x.replace('COVID-19','COVID 19')
        x=x.replace('HAED SURTURING RIDGING','HEAD SURTURING RIDGING')
        x=x.replace('INTRAHEPATIC CHOLESTATIS','CHOLESTATIS')
        x=x.replace('LAE PRETERM','LATE PRETERM')
        x=x.replace('LESS FETAL MOVEMENT','FAILURE OF FETAL EFFORTS')
        x=x.replace('HYPERBILIRUBINEMIA','NEONATAL HYPERBILIRUBINEMIA')
        x=x.replace('RIGHT PUJ OBSTRUCTION','PELVIC URETERIC JUNCTION OBSTRUCTION')
        x=x.replace('SALPINGECTOMY','SALPHINGECTOMY')
        x=x.replace('YELLOWISH DISCOLORATION OF BODY','YELLOWISH DISCOLORATION')
        x=x.replace('YELLOWISH DISCOLORATION OF EYES AND BODY','YELLOWISH DISCOLORATION')
        x=x.replace('YELLOWISH DISCOLORATION OF SKIN','YELLOWISH DISCOLORATION')
        x=x.replace('YELLOWISH DISCOLORATION OF SKIN AND EYES','YELLOWISH DISCOLORATION')
        x=x.replace('YELLOWISH DISCOLORATION OF SKIN AND SCLERA','YELLOWISH DISCOLORATION')
        x=x.replace('YELLOWISH DISCOLORATION OF THE BODY','YELLOWISH DISCOLORATION')
        x=x.replace('YELLOWISH DISCOLORATION AND EYES','YELLOWISH DISCOLORATION')
        x=x.replace('YELLOWISH DISCOLORATION AND SCLERA','YELLOWISH DISCOLORATION')
        x=x.replace('VACCUM DELIVERY','VACCUM VAGINAL DELIVERY')
        x=x.replace('NORMAL DELIVERY','NORMAL VAGINAL DELIVERY')
        x=x.replace('AFO','AFI')
        x=x.replace('PRPTHROMBIN TEST','PROTHROMBIN TEST')

        
        
        x=x.split(',')
        x=list(map(lambda y:y.strip(),x))
        x=list(map(lambda y:y.upper(),x))
        return x
    else:
        return x
        
        
    


# %%
motherhood_data['past_history']=motherhood_data['past_history'].apply(lambda x:reform_list(x))


# %%
motherhood_data['diagnosis']=motherhood_data['diagnosis'].apply(lambda x:reform_list(x))


# %%
motherhood_data['symptoms']=motherhood_data['symptoms'].apply(lambda x:reform_list(x))


# %%
motherhood_data['procedure']=motherhood_data['procedure'].apply(lambda x:reform_list(x))


# %%
motherhood_data['tests']=motherhood_data['tests'].apply(lambda x:reform_list(x))


# %%
motherhood_data['Type_of_pregnancy']=motherhood_data['Type_of_pregnancy'].replace('[PRIMIGRAVIDA','PRIMIGRAVIDA')
motherhood_data['Type_of_pregnancy']=motherhood_data['Type_of_pregnancy'].replace('primigravida','PRIMIGRAVIDA')

# %%


# %%
#finding unique complications

list_of_unique_complications=list(set(itertools.chain.from_iterable(motherhood_data[motherhood_data['tests'].isnull()==False]['tests'])))

# %%
list_of_unique_complications.sort()

# %%
#grpuping columns to check datatype
motherhood_data.columns.to_series().groupby(motherhood_data.dtypes).groups

# %%
#defining list of columns that will be added,aggregated ,append or used to find maximum
cols_for_aggregate=['cpid','icu_rent_from_claim_details_no_capping','flag_for_admission_period_within_policy_period',
        'icu_rent_from_claim_details_nan','uid_claim_id','claim_type_from_claim_details',
        'hospital_legal_name_from_claim_details',
        'hospital_display_name_from_claim_details',
        'treatment_name_from_claim_details',
        'standard_treatment_name_from_claim_details',
        'room_category_name_from_claim_details', 'agerange',
        'past_history_from_discharge_summary', 'cause_from_discharge_summary',
        'symptom_from_discharge_summary', 'Type_of_pregnancy', 
        'course_from_discharge_summary', 'relationship_from_insurance_details',
        'discharge_date_from_claim_details',
        'admission_date_from_claim_details','discount_from_final_bill',
        'policyStartDate_from_insurance_details',
        'policyEndDate_from_insurance_details',
        'inceptionDate_from_insurance_details',
        'card_info_from_insurance_details', 'Treatment_sublimit',
        'waiting_period', 'pre_hosp_expense_in_days',
        'post_hosp_expense_in_days', 'maternity_c_Section_sublimit',
        'treatment_remarks',
        'reasons_for_deduction_from_settlement_letter_table',
        'predicted_data_from_settlement_letter_table',
        'deduction_reasons_from_settlement_letter_table', 'policy-Primary_member_from_insurance_details',
        'policy-Relationship-None_from_insurance_details']

cols_for_adding=['claim_amount_from_claim_details','room_rent_from_claim_details','Hospitalization_amount_from_claim_details',
        'PostH_amount_from_claim_details', 'PreH_amount_from_claim_details',
        'Total_amount_from_claim_details', 'pphc_amount_from_claim_details',
        'Diagnostic Bill Amount', 'Discharge Summary Amount',
        'Final Bill Amount', 'Final Bill Breakup Amount', 'ID Proof Amount',
        'Lab Reports Amount', 'OPD Consultation Bill Amount',
        'Pharmacy Bill Amount', 'Physiotherapy Bill Amount',
        'Prescriptions Amount',
         'fees_from_doctors',
        'total_bill_amount_from_final_bill',
        'insurer_paid_amount_from_final_bill',
        'patient_paid_amount_from_final_bill',
        'room_days_from_final_bill', 'room_rent_from_final_bill',
        'room_nursing_charge_from_final_bill',
        'room_duty_doctor_charge_from_final_bill', 'dr_charge_from_final_bill',
        'ot_charge_from_final_bill', 'asst_dr_charge_from_final_bill',
        'implant_charge_from_final_bill', 'service_charge_from_final_bill',
        'anathesia_charge_from_final_bill', 'equipment_charge_from_final_bill',
        'consumable_amount_from_final_bill', 'medicine_amount_from_final_bill',
        'surgery_package_charge_from_final_bill','icu_rent_from_claim_details_cap_amount','icu_days_from_final_bill', 'icu_rent_from_final_bill',
        'icu_nursing_charge_from_final_bill',
        'icu_duty_doctor_charge_from_final_bill',
        'total_claim_amount_from_settlement_letter_table',
        'total_deducted_amount_from_settlement_letter_table',
        'total_settled_amount_from_settlement_letter_table']


cols_for_max=['admission_duration','age_from_claim_details','total_policy_from_insurance_details','total_sum_insured_from_insurance_details',
        'available_sum_insured_from_insurance_details',
        'time_in_years_from_insurance_details', 'policy_duration','experience_in_years_from_doctors',
        'policy_inception_wrt_start']
cols_for_append=['past_history','diagnosis','symptoms','procedure','tests']

# %%


# %%


# %%
motherhood_data['past_history']=motherhood_data['past_history'].fillna('NA')

# %%
motherhood_data['diagnosis']=motherhood_data['diagnosis'].fillna('NA')

# %%
motherhood_data['symptoms']=motherhood_data['symptoms'].fillna('NA')

# %%
motherhood_data['procedure']=motherhood_data['procedure'].fillna('NA')

# %%
motherhood_data['tests']=motherhood_data['tests'].fillna('NA')

# %%
motherhood_data['agerange']=motherhood_data['agerange'].fillna('NA')
motherhood_data['Type_of_pregnancy']=motherhood_data['Type_of_pregnancy'].fillna('NA')


# %%
#creating dictionary of functions for the above lists of columns
dict_for_max=dict.fromkeys(cols_for_max,'max') 
dict_for_add=dict.fromkeys(cols_for_adding,'sum') 
dict_for_agg=dict.fromkeys(cols_for_aggregate,lambda x: list(x)) 
dict_for_append=dict.fromkeys(cols_for_append,lambda x:list(x)) 


# %%
#extending dict into one
dict_for_max.update(dict_for_add)
#extending dict into one to be supplied to aggregate function
dict_for_max.update(dict_for_agg)
#extending dict into one to be supplied to aggregate function
dict_for_max.update(dict_for_append)

# %%
#grouping data family id wise and applying functions to columns
motherhood_data_family_wise=motherhood_data.groupby('family_id').agg(dict_for_max)

# %%
motherhood_data_family_wise['age_from_claim_details']=motherhood_data_family_wise['age_from_claim_details'].fillna(-1)

# %%
# pd.qcut(motherhood_data_family_wise['age_from_claim_details'],q=3).value_counts()

# %%
motherhood_data_family_wise['age_range']=pd.qcut(motherhood_data_family_wise['age_from_claim_details'],q=3)
motherhood_data_family_wise['age_range']=motherhood_data_family_wise['age_range'].apply(lambda x:str(x))

# %%
#selected fields for CART
motherhood_data_family_wise_selected=motherhood_data_family_wise[[
 'room_category_name_from_claim_details','Total_amount_from_claim_details','age_range',
 'age_from_claim_details','agerange','admission_duration','past_history','diagnosis','symptoms','procedure','tests','Type_of_pregnancy','hospital_display_name_from_claim_details'
 ]]


# %%
# motherhood_data_family_wise_selected

# %%
#taking unique of agerange 
motherhood_data_family_wise_selected['agerange']=motherhood_data_family_wise_selected['agerange'].apply(lambda x:list(set(x)))

# %%
#taking unique of room category types in a list of room categories
motherhood_data_family_wise_selected['room_category_name_from_claim_details']=motherhood_data_family_wise_selected['room_category_name_from_claim_details'].apply(lambda x:list(set(x)))

# %%
#taking unique of room category types in a list of room categories
motherhood_data_family_wise_selected['hospital_display_name_from_claim_details']=motherhood_data_family_wise_selected['hospital_display_name_from_claim_details'].apply(lambda x:list(set(x)))

# %%
#taking unique of room category types in a list of type of pregnancy
motherhood_data_family_wise_selected['Type_of_pregnancy']=motherhood_data_family_wise_selected['Type_of_pregnancy'].apply(lambda x:list(set(x)))

# %%
#function to flatten irregular list
def flatten(container):
    for i in container:
        if isinstance(i, (list,tuple)):
            for j in flatten(i):
                yield j
        else:
            yield i


# %%
def unique_comp(x):
    
    if any(isinstance(i, list) for i in x):
        try:
            x=sum(x,[])
            x=list(set(x))
            return x
        except:
            x=list(flatten(x))
            x=list(set(x))
            return x
    else:
        x=list(set(x))
        return x
    
    

# %%
motherhood_data_family_wise_selected['past_history']=motherhood_data_family_wise_selected['past_history'].apply(lambda x:unique_comp(x))
motherhood_data_family_wise_selected['diagnosis']=motherhood_data_family_wise_selected['diagnosis'].apply(lambda x:unique_comp(x))
motherhood_data_family_wise_selected['symptoms']=motherhood_data_family_wise_selected['symptoms'].apply(lambda x:unique_comp(x))
motherhood_data_family_wise_selected['procedure']=motherhood_data_family_wise_selected['procedure'].apply(lambda x:unique_comp(x))
motherhood_data_family_wise_selected['tests']=motherhood_data_family_wise_selected['tests'].apply(lambda x:unique_comp(x))

# %%
motherhood_data_family_wise_selected.columns

# %%
motherhood_data_family_wise_selected.shape

# %%
"""
# bayesian network
"""

# %%
maternity_data_for_bayes=motherhood_data_family_wise_selected.copy()

# %%

def formatting_top(row):
    if not 'NA' in row['Type_of_pregnancy']:
#         print(row['Type_of_pregnancy'][0])
        return row['Type_of_pregnancy'][0]
       
        
    else:
#         print(row['Type_of_pregnancy'][0])
        return row['Type_of_pregnancy'][0]

# %%
maternity_data_for_bayes['Type_of_pregnancy']=maternity_data_for_bayes.apply(lambda x:formatting_top(x),axis=1)

# %%
def formatting_agerange(row):

    if not 'NA' in row['agerange']:
        if '0-9' in row['agerange'] and len(row['agerange'])>1:
            row['agerange'].remove('0-9')
            
#             print(row['agerange'][0])
            return row['agerange'][0]
        else:
#             print(row['agerange'][0])
            return row['agerange'][0]
        
    else:
#         print(row['agerange'][0])
        return row['agerange'][0]

# %%
maternity_data_for_bayes['agerange']=maternity_data_for_bayes.apply(lambda x:formatting_agerange(x),axis=1)

# %%


# %%
#one hot encoding of past_history
ohe_data1=maternity_data_for_bayes['past_history'].astype(str).str.strip('[]').str.get_dummies(', ')
ohe_data1=ohe_data1.add_prefix('past_history_')

#one hot encoding of diagnosis
ohe_data2=maternity_data_for_bayes['diagnosis'].astype(str).str.strip('[]').str.get_dummies(', ')
ohe_data2=ohe_data2.add_prefix('diagnosis_')

#one hot encoding of symptoms
ohe_data3=maternity_data_for_bayes['symptoms'].astype(str).str.strip('[]').str.get_dummies(', ')
ohe_data3=ohe_data3.add_prefix('symptoms_')

#one hot encoding of procedure
ohe_data4=maternity_data_for_bayes['procedure'].astype(str).str.strip('[]').str.get_dummies(', ')
ohe_data4=ohe_data4.add_prefix('procedure_')

#one hot encoding of tests
ohe_data5=maternity_data_for_bayes['tests'].astype(str).str.strip('[]').str.get_dummies(', ')
ohe_data5=ohe_data5.add_prefix('tests_')

#one hot encoding of room category
ohe_data6=maternity_data_for_bayes['room_category_name_from_claim_details'].astype(str).str.strip('[]').str.get_dummies(', ')
ohe_data6=ohe_data6.add_prefix('room_category_')

#one hot encoding of hospital
ohe_data7=maternity_data_for_bayes['hospital_display_name_from_claim_details'].astype(str).str.strip('[]').str.get_dummies(', ')
ohe_data7=ohe_data7.add_prefix('hospital_name_')

#one hot encoding of pregnacy type
ohe_data8=maternity_data_for_bayes['Type_of_pregnancy'].astype(str).str.strip('[]').str.get_dummies(', ')
ohe_data8=ohe_data8.add_prefix('pregnancy_type_')

#one hot encoding of agerange
ohe_data9=maternity_data_for_bayes['agerange'].astype(str).str.strip('[]').str.get_dummies(', ')
ohe_data9=ohe_data9.add_prefix('agerange_')
ohe_data9=ohe_data9.add_suffix('_bucket')




# %%
maternity_data_for_bayes.columns

# %%
# ohe_data9.shape

# %%
maternity_data_for_bayes=maternity_data_for_bayes.drop(columns={'room_category_name_from_claim_details','age_from_claim_details',
                                                             'tests', 'hospital_display_name_from_claim_details','agerange' })

# %%
maternity_data_for_bayes=pd.concat([maternity_data_for_bayes,ohe_data1],axis=1)
maternity_data_for_bayes=pd.concat([maternity_data_for_bayes,ohe_data2],axis=1)
maternity_data_for_bayes=pd.concat([maternity_data_for_bayes,ohe_data3],axis=1)
maternity_data_for_bayes=pd.concat([maternity_data_for_bayes,ohe_data4],axis=1)
# maternity_data_for_bayes=pd.concat([maternity_data_for_bayes,ohe_data5],axis=1)
# maternity_data_for_bayes=pd.concat([maternity_data_for_bayes,ohe_data6],axis=1)
# maternity_data_for_bayes=pd.concat([maternity_data_for_bayes,ohe_data7],axis=1)
# maternity_data_for_bayes=pd.concat([maternity_data_for_bayes,ohe_data8],axis=1)
# maternity_data_for_bayes=pd.concat([maternity_data_for_bayes,ohe_data9],axis=1)



# %%
maternity_data_for_bayes=maternity_data_for_bayes.drop(columns={'past_history','symptoms',"diagnosis_'NA'",
       'procedure',"past_history_'NA'","symptoms_'NA'","procedure_'NA'","diagnosis"})

# %%
maternity_data_for_bayes.shape

# %%
#find columns with missing values
list_of_cols_with_nan=maternity_data_for_bayes.columns[maternity_data_for_bayes.isna().any()].tolist()
pd.DataFrame(maternity_data_for_bayes[list_of_cols_with_nan].isna().sum())

# %%
maternity_data_for_bayes_network=maternity_data_for_bayes.drop(columns={'Total_amount_from_claim_details'})
maternity_data_for_bayes_network=pd.concat([maternity_data_for_bayes_network,maternity_data_for_bayes['Total_amount_from_claim_details']],axis=1)


# %%
maternity_data_for_bayes_network.shape

# %%
# maternity_data_for_bayes_network.columns.tolist()

# %%
# def calculate_rmse(predicted_prob_data):
    
#     diff1=[]
#     diff2=[]
#     diff3=[]

#     diff1.append(((predicted_prob_data['actual_probability']-predicted_prob_data['predicted_probability_exact'])**2).tolist())
#     diff2.append(((predicted_prob_data['actual_probability']-predicted_prob_data['predicted_probability_chow'])**2).tolist())
#     diff3.append(((predicted_prob_data['actual_probability']-predicted_prob_data['predicted_probability_own_DAG'])**2).tolist())

#     diff1=sum(diff1,[])
#     diff2=sum(diff2,[])
#     diff3=sum(diff3,[])


#     diff1=list(filter(lambda x: str(x) != 'nan',diff1))
#     diff2=list(filter(lambda x: str(x) != 'nan',diff2))
#     diff3=list(filter(lambda x: str(x) != 'nan',diff3))

#     rmse_exact=np.sqrt(mean(diff1))
#     rmse_chow=np.sqrt(mean(diff2))
#     rmse_own_DAG=np.sqrt(mean(diff3))
    
#     return(rmse_exact,rmse_chow,rmse_own_DAG)



# %%

# use_case=use_case1.drop(columns={'age_range', 'Type_of_pregnancy'})
# from sklearn.utils.class_weight import compute_class_weight

# # def calculating_class_weights(y):
# number_dim = np.shape(np.array(use_case))[1]

# weights = np.empty([number_dim, 2])
# for i in range(number_dim):
    
#     weights[i] = compute_class_weight('balanced',[0,1],np.array(use_case)[:,i])

# column_weights=[]
# column_weights.append(1)
# column_weights.append(1)

# for i in weights:
#     column_weights.append(i[1])


# %%
"""
# pomegranate
"""

# %%


# %%
# help(BayesianNetwork.from_samples)

# %%
maternity_data_for_bayes_network['total_amount_network']=pd.qcut(maternity_data_for_bayes_network['Total_amount_from_claim_details'],q=10)
maternity_data_for_bayes_network['total_amount_network']=maternity_data_for_bayes_network['total_amount_network'].apply(lambda x:str(x))

# %%
# maternity_data_for_bayes_network['total_amount_network'].value_counts()

# %%
# maternity_data_for_bayes_network['total_amount_network']=pd.qcut(maternity_data_for_bayes_network['Total_amount_from_claim_details'],q=10)
# maternity_data_for_bayes_network['total_amount_network']=maternity_data_for_bayes_network['total_amount_network'].apply(lambda x:str(x))

# %%
# pd.qcut(maternity_data_for_bayes_network['admission_duration'],q=3).value_counts()

# %%
# maternity_data_for_bayes_network.columns.tolist()

# %%


# %%
use_case1=maternity_data_for_bayes_network[['age_range',
 'Type_of_pregnancy', "past_history_'AMENORRHOEA'","past_history_'HYPOTHYROIDISM'","past_history_'GDM'",
"past_history_'PREVIOUS LSCS'","diagnosis_'FETAL DISTRESS'","diagnosis_'NEONATAL HYPERBILIRUBINEMIA'","diagnosis_'NEONATAL JAUNDICE'","diagnosis_'NON PROGRESS OF LABOUR'","diagnosis_'PRETERM'",
   "diagnosis_'OLIGOHYDRAMNIOS'", "procedure_'ELECTIVE LSCS'",
 "procedure_'EMERGENCY LSCS'", "procedure_'FTND'","procedure_'RMLE'","procedure_'LSCS'","procedure_'NORMAL VAGINAL DELIVERY'","procedure_'VACCUM VAGINAL DELIVERY'","procedure_'TUBECTOMY'"]]

# %%
use_case1.shape

# %%
use_case_1=use_case1.values

# %%


# %%
"""
models for testing
"""

# %%
"""
penalty 1
"""

# %%
model_exact_uc1 = BayesianNetwork.from_samples(use_case_1, algorithm='exact',state_names=use_case1.columns,penalty=1)
model_exact_uc1.structure

# %%
# print(mean(rmse_exact))

# %%
# model_exact_exp5.plot(filename='exact_exp5.pdf')

# %%


# %%
"""
network with amounts
"""

# %%
use_case2=maternity_data_for_bayes_network[['age_range',
 'Type_of_pregnancy', "past_history_'AMENORRHOEA'","past_history_'HYPOTHYROIDISM'","past_history_'GDM'",
"past_history_'PREVIOUS LSCS'","diagnosis_'FETAL DISTRESS'","diagnosis_'NEONATAL HYPERBILIRUBINEMIA'","diagnosis_'NEONATAL JAUNDICE'","diagnosis_'NON PROGRESS OF LABOUR'","diagnosis_'PRETERM'",
   "diagnosis_'OLIGOHYDRAMNIOS'", "procedure_'ELECTIVE LSCS'",
 "procedure_'EMERGENCY LSCS'", "procedure_'FTND'","procedure_'RMLE'","procedure_'LSCS'","procedure_'NORMAL VAGINAL DELIVERY'","procedure_'VACCUM VAGINAL DELIVERY'","procedure_'TUBECTOMY'",'total_amount_network']]

# %%
use_case2.shape

# %%
use_case_2=use_case2.values

# %%
model_exact_uc2 = BayesianNetwork.from_samples(use_case_2, algorithm='exact',state_names=use_case2.columns,penalty=1)
model_exact_uc2.structure

# %%


# %%
#model save

# %%
x1 = model_exact_uc1.to_json()
f = open("bayesian_network_without_amt.txt", "w")
f.write(x1)
f.close()

# %%
x1 = model_exact_uc2.to_json()
f = open("bayesian_network_amt.txt", "w")
f.write(x1)
f.close()

# %%


# %%
# maternity_data_for_bayes_network['age_range'].value_counts()

# %%


# %%
#model read

# %%

# d = open("bayesian_network_without_amt.txt", "r")
# model_exact_trial = from_json(d.read())

# %%


# %%
# beliefs=model_exact_trial.predict_proba({"past_history_'HYPOTHYROIDISM'":1,"past_history_'GDM'":1})
# # value1=belief1[ind].parameters[0][row[-3]]
# beliefs = map(str, beliefs)
# print("\n".join( "{}\t{}".format( state.name, belief ) for state, belief in zip( model_exact_trial.states, beliefs ) ))
 

# %%


# %%
# model_exact_exp16.plot(filename='exact_exp16.pdf')

# %%
# rmse_exact=[]
# rmse_chow=[]
# rmse_own_DAG=[]
# c=0
# for i in feat_combo:
#     feat=list(i)
#     print(c)
#     actual_prob_data=calculate_prob(use_case2,feat[0:-1],feat[-1])
#     predicted_prob_data=actual_prob_data[str(feat)].copy()
#     ind=actual_prob_data['index']
#     predicted_prob_data[['predicted_probability_exact','predicted_probability_chow','predicted_probability_own_DAG']]=predicted_prob_data.apply(lambda x:predict_from_model(model_exact_exp16,model_chow_uc2,model_own_DAG_uc2,x,ind),axis=1)
#     print(feat)
#     print('\n')
#     print(predicted_prob_data)
#     print("*************************")
#     print("\n")
    
#     re,rc,rd=calculate_rmse(predicted_prob_data)
#     rmse_exact.append(re)
#     rmse_chow.append(rc)
#     rmse_own_DAG.append(rd)
#     c=c+1

# %%
# print(mean(rmse_exact))

# %%


# %%
"""
# finding probablities
"""

# %%


# %%
# def predict_from_model(model1,model2,model3,row,ind):
   
#     belief1=model1.predict_proba(row[0:-3].to_dict())
#     value1=belief1[ind].parameters[0][row[-3]]
    
#     belief2=model2.predict_proba(row[0:-3].to_dict())
#     value2=belief2[ind].parameters[0][row[-3]]
    
#     belief3=model3.predict_proba(row[0:-3].to_dict())
#     value3=belief3[ind].parameters[0][row[-3]]
    
#     return pd.Series([value1,value2,value3],index=['predicted_probability_exact','predicted_probability_chow','predicted_probability_own_DAG'])
    

# %%
# from statistics import mean 
# from operator import itemgetter
# from get_conditional_probability import calculate_prob

# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%


# %%
