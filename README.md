### bayesian network for maternity data ###
This api accepts set of parameters like past history,diagnosis,procedures etc as input and give probabilities of occurance other parameters  


### Installation and run instructions###

* clone the repository
* run pip install -r requirements.txt
* run `predict_api.py

### How to call api ###

* Endpoint 1 (expects set of parameters as input and give probabilities of all the other features)
	* http://0.0.0.0:5000/all
	* Sample json to send request:
		* {"diagnosis":["cpd"],"past_history":["hypertension"],"pregnancy_type":["primigravida"],"procedure":["elective LSCS"],"symptoms":["pain abdomen"],"room_category":"general","age":24,"los":4}
		
	* Sample output:
			Dictionary
	
* Endpoint 2 (expects set of parameters as input and give probabilities of specific fetaures asked)
	* http://0.0.0.0:5000/specific
	* Sample json to send request:
		* {"diagnosis":["cpd"],"past_history":["hypertension"],"pregnancy_type":["primigravida"],"procedure":["elective LSCS"],"symptoms":["pain abdomen"],"room_category":"general","age":24,"los":4,"predict":["procedure","diagnosis"]}
		
	* Sample output:
			Dictionary	
		
